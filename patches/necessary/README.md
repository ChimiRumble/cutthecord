## DisTok CutTheCord: Necessary Patches

These patches are used in cases where discord fucks up and repacks fail.

If the version you're using is in the following list, please make sure to use the relevant patch file before repacking.

#### Side effects / Bugs
- 8.3.1-Current: Prevents changing a role color

#### Available and tested on:
- 8.3.1
- 8.3.2
- 8.3.3
- 8.3.4g
- 8.3.5g
- 8.3.6g
- 8.3.9g
- 8.4.1g
- 8.4.2g
- 8.4.3g
- 8.4.4g
- 8.4.5g
- 8.4.8
- 8.5.0
- 8.5.1
- 8.5.3
- 8.5.4
- 8.5.5

As of apktool v2.4, the current type of necessary patches aren't needed and won't be included, therefore defeating the bugs introduced by it.

