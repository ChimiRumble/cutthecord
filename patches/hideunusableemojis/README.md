## DisTok CutTheCord: Hide Unusable Emojis Patch

This patch only shows the emojis the user can actually use (mostly hides custom emojis from other guilds when you don't have Nitro).

![](https://lasagna.cat/i/4g6wfksl.png)

#### Thanks

Big thanks to [@BlueMods](https://gitdab.com/BlueMods) for sending in this patch.

#### Available and tested on:
- 34.0
- 34.2
- 34.3
- 35.0-alpha1

